document.write("Задание<br>Разработайте функцию-конструктор, которая будет создавать объект Human (человек). Создайте массив объектов и реализуйте функцию, которая будет сортировать элементы массива по значению свойства Age по возрастанию или по убыванию.<br><br>");

// Human constructor
function Human (name, surname, age) {
    this.name = name;
    this.surname = surname;
    this.age = age;
}

// sort array of objects by age
function sortByAge (arr) {
    arr.sort(function(a, b){
        return a.age-b.age;
    });
}

// sort array of objects by age reverse
function sortByAgeReverse (arr) {
    arr.sort(function(a, b){
        return a.age-b.age;
    }).reverse();
}

// show array of objects
function show (arr) {
    arr.forEach(element => {
        document.write(`${element.name} ${element.surname} ${element.age}<br>`);
    });
}

const john = new Human("John", "Doe", 25);
const sam = new Human("Sam", "Smith", 18);
const alice = new Human("Alice", "Wood", 22);

const arr = [john, sam, alice];

document.write("Массив объектов:<br>");
show(arr);

document.write("<br>Массив объектов отсортирован по возрасту по возрастанию:<br>");
sortByAge(arr);
show(arr);

document.write("<br>Массив объектов отсортирован по возрасту по убыванию:<br>");
sortByAgeReverse(arr);
show(arr);