document.write("Задание<br>Разработайте функцию-конструктор, которая будет создавать объект Human (человек), добавьте на свое усмотрение свойства и методы в этот объект. Подумайте, какие методы и свойства следует сделать уровня экземпляра, какие уровня функции-конструктора.<br><br>");

// Human constructor
function Human (name, surname, age) {
    this.name = name;
    this.surname = surname;
    this.age = age;
}

// property of constructor function
Human.minAge = 0;

// method of constructor function
Human.getSelf = function () {
    return new Human("Vika", "Babyak", 22);
}

// method of exemplar
Human.prototype.show = function () {
    document.write(`${this.name} ${this.surname} ${this.age}<br>`);
}

const john = new Human("John", "Doe", 25);
const sam = new Human("Sam", "Smith", 18);
const alice = new Human("Alice", "Wood", 21);

john.show();
sam.show();
alice.show();


Human.getSelf().show();
document.write(Human.minAge);
